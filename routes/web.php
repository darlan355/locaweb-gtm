<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('loginGTM');
})->name('login');

Route::get('/workspaces', 'WorkspacesController@index')->name('listaWorkspaces');

Route::post('/workspaces/selecionar', 'WorkspacesController@selecionar');

Route::get('/integracoes', 'IntegracoesController@index')->name('listaIntegracoes');

Route::get('/integracoes/ecommerce', 'IntegracoesController@dadosEcommerce')->name('dadosEcommerce');

Route::get('/integracoes/tags/personalizadas', 'IntegracoesController@listaTagsIndex')->name('listaIntegracoesTags');
