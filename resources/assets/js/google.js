class Google {
    
   static initLib(callback){
        if(auth2){
            callback();
        }
        else{
            gapi.load('auth2', callback);    
        }
    }
    
    static login(callbackSucess, callbackFail){
          Google.initLib( function () {
                    auth2 = gapi.auth2.init({
                        client_id: '752648310447-leljc54e7nbgkcjtk4jgq5b2bqu1o7p6.apps.googleusercontent.com',
                        fetch_basic_profile: true,
                        scope: 'profile'
                    });
                    
                     auth2.signIn().then(function() {
                        onSignIn(auth2.currentUser.get());
                        callbackSucess()
                    },callbackFail);
                })
    }
    
    static onSignIn(googleUser) {

          var user ={};
          var profile = googleUser.getBasicProfile();
          
          console.log(profile);
          
          user.id = profile.getId();
          user.name=  profile.getName();
          user.email= profile.getEmail();
          
          document.location= '\workspaces';
        }
}

