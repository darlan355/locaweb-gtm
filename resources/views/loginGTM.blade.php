@extends('integracoesmaster')

@section('main')
<div class="ls-txt-center-all">
    <p>
        <img src="{{asset('img/gtm_image.png') }}">
    </p>
    <p>
        <h3 class="ls-txt-center ls-text-uppercase">faça o login no gtm</h3>
    </p>
    <p class="ls-txt-center">
        <a href="#" class="ls-btn-primary" id="btnAtivar" onclick="signIn()">Clique para Ativar</a>
    </p>
</div>



<script type='text/javascript'>
    function onSignIn(googleUser) {

        var profile = googleUser.getBasicProfile();
        /*console.log(profile);
        console.log('ID: ' + profile.getId());
        console.log('Name: ' + profile.getName());
        console.log('Email: ' + profile.getEmail());  */

        console.log(googleUser);
        console.log(googleUser.getAuthResponse());

        var id = profile.getId();
        var token = googleUser.getAuthResponse().id_token;
        document.location = '\workspaces?token=' + token + '&id=' + id;
    }

    function signIn() {
        showLoading();

        auth2.signIn().then(function () {
            onSignIn(auth2.currentUser.get());
        }, function () {
            modalError('Usuário ou senha inválidos!');
        });
    }
</script>
@stop