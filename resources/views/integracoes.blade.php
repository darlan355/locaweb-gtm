@extends('integracoesmaster')

@section('main')
<div class="ls-box">
    <div class="row">
        <div class="col-md-10">
            <h5 class="ls-title-3">Workspace</h5>
            <p><?php echo $workspace_name; ?>
            </p>
        </div>
        <div class="col-md-2">
            <a onclick="showLoading();" href="<?php echo $url_trocarworkspace; ?>" class="ls-btn">Trocar workspace</a>
        </div>
    </div>
</div>


<input type="hidden" id="hd_workspace" value="<?php echo $workspace;  ?>" />
<div class="ls-alert-info ls-sm-space">Selecione a integração desejada</div>

<div class="row">

    <div class="col-md-3 ls-sm-space">
        <div class="ls-box ">
            <div class="ls-box-head">
                <h1 class="ls-title-3 ls-txt-center">
                    <a data-ls-module="modal" data-target="#modal-confirma-integrar" href="#" data="vtex" class="link">
                        <span style="color:#0D5285">VTE</span><span style="color:#DC8C26">X</span>
                </h1>
                </a>
            </div>
            <div class="ls-box-body ls-txt-center">

            </div>
        </div>
    </div>


    <div class="col-md-3 ls-sm-space">
        <div class="ls-box ">
            <div class="ls-box-head">
                <h1 class="ls-title-3 ls-txt-center">
                    <a data-ls-module="modal" data-target="#modal-confirma-integrar" href="#" data="tray" class="link">
                        <span style="color:#1C3F76">TRAY</span>
                    </a>
                </h1>
            </div>
            <div class="ls-box-body">

            </div>
        </div>
    </div>


    <div class="col-md-3 ls-sm-space">
        <div class="ls-box ">
            <div class="ls-box-head">
                <h1 class="ls-title-3 ls-txt-center">
                    <a data-ls-module="modal" data-target="#modal-confirma-integrar" href="#" data="fbits" class="link">
                        <span style="color:#A2BA20">FBITS</span>
                    </a>
                </h1>
            </div>
            <div class="ls-box-body">

            </div>
        </div>
    </div>


    <div class="col-md-3 ls-sm-space">
        <div class="ls-box ">
            <div class="ls-box-head">
                <h1 class="ls-title-3 ls-txt-center">
                    <a data-ls-module="modal" data-target="#modal-confirma-integrar" href="#" data="rakuten" class="link">
                        <span style="color:#B72722">RAKUTEN</span>
                    </a>
                </h1>
            </div>
            <div class="ls-box-body">

            </div>
        </div>
    </div>


    <div class="col-md-3 ls-sm-space">
        <div class="ls-box ">
            <div class="ls-box-head">
                <h1 class="ls-title-4 ls-txt-center">
                    <a href="<?php echo $url_tagspersonalizadas;  ?>">
                    <span>PERSONALIZADO</span>
                    </a>
                </h1>
            </div>
            <div class="ls-box-body">

            </div>
        </div>
    </div>

</div>

<div class="ls-modal" id="modal-confirma-integrar">
    <div class="ls-modal-small">
        <div class="ls-modal-header">
            <button data-dismiss="modal">&times;</button>
            <h4 class="ls-modal-title">Atenção</h4>
        </div>
        <div class="ls-modal-body">
            <p>Deseja integrar o item selecionado com o GTM?</p>
        </div>
        <div class="ls-modal-footer">
            <button class="ls-btn ls-float-right" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="ls-btn-primary" onclick="processTag()">Sim</button>
        </div>
    </div>
</div>



<script>
        var urlBuscaDadosEcommerce = '<?php echo(route('dadosEcommerce')) ?>';
</script>

<script type="text/javascript">

    var workspace = document.getElementById('hd_workspace').value;
    var ecommerce = '';
    var btn = null;
    var tags = [];
    var variables = [];
    var tagsCreated = [];
    var triggersCreated = [];
    var tagsUpdate = [];
    var triggers = [];

    window.onload = function () {
        loadTagManagerApi();

        $('body').on('modal:opened', '#modal-confirma-integrar', function (evt, button) {
            var btn$ = $(button);
            ecommerce = btn$.attr('data');
            btn = btn$;
        });
    }

    /*
     * Init Process create tags
     */
    function processTag() {
        clear();
        locastyle.modal.close('#modal-confirma-integrar');

        $('.ls-alert-success').remove();
        var div = btn.parent().parent().parent().parent().find('.ls-box-body');
        $(div).append('<div class="ls-alert-success ls-sm-space">EM USO</div>');

        showLoading();
        loadTagManagerApi()
            .then(getDataEcommerce)
            .then(listTriggers)
            .then(listTags)
            .then(createTag)
            .then(createTrigger)
            .then(updateTagWithTrigger)
            .then(listVariables)
            .then(createVariables)
            .then(finish)
            .catch((ret) => {
                console.log(ret);
                modalError('Ops, ocorreu um problema. \n' + (ret.message ? ret.message : ret));
            })
    }

    /*
     *  Clear variables
     */ 
    function clear() {
        tags = [];
        variables = [];
        tagsCreated = [];
        triggersCreated = [];
        tagsUpdate = [];
        triggers = [];
    }

    /*
     *  Get data from api of ecommerces
     */
    function getDataEcommerce() {

        return new Promise((resolve, reject) => {

            var request = $.ajax(urlBuscaDadosEcommerce + '?ecommerce=' + ecommerce);

            $.when(request).then((response) => {
                clear();

                tags = response.tags;
                variables = response.variables;

                return resolve();
            }).fail((response) => {
                return reject(response);
            });
        });
    }

    /*
    * Finish process
    */
    function finish() {
        dismissLoading();
        modalError('Integração realizada com sucesso!');
    }


    /**
 * Creates all page view trigger.
 */
    function createTrigger() {
        var promisses = [];
        debugger;
        return new Promise((resolve, reject) => {
            for (var i = 0; i < tagsCreated.length; i++) {

                 var triggerFound =triggers.find((tr) => {
                     return tr.name == tagsCreated[i].name;
                });

                if (!triggerFound) {
                    var trigger = { 'name': tagsCreated[i].name, 'type': 'PAGEVIEW' };
                    var request = gapi.client.tagmanager.accounts.containers.workspaces.triggers.create({ 'parent': workspace }, trigger);
                    promisses.push(requestPromise(request));
                }
            }

            Promise.all(promisses).then((responses) => {
                triggersCreated = responses;

                console.log('Triggers criadas ', triggersCreated);
                return resolve();
            }).catch((ret) => {
                return reject(ret.message);
            });

        });
    }

    /**
    * Creates a universal analytics tag.
    *
    */
    function createTag(listTags) {

        return new Promise((resolve, reject) => {
            console.log('tags ', this.tags)
            var tmp_listTags = listTags.tag || [];
            debugger;
            var promisses = [];
      

            for (var i = 0; i < this.tags.length; i++) {
                var tagServer = tags[i];

                var tag = {
                    'name': tagServer.name,
                    'type': 'html',
                    'parameter': [{
                        'key': 'html', 'type': 'template', 'value': tagServer.value
                    }]
                };

                debugger;
                var tagFound = tmp_listTags.find((currentValue) => {
                    return currentValue.name == tagServer.name;
                });


                // not exists variable in server
                if (tagFound) {

                    var haveChanges = diffTag(tagFound, tagServer);

                    if (haveChanges) {

                        //method update
                        tag['firingTriggerId'] = [tagFound.firingTriggerId];
                        var request = gapi.client.tagmanager.accounts.containers.workspaces.tags.update({ 'path': tagFound.path }, tag);
                        promisses.push(requestPromise(request));
                        tagsUpdate.push(tagFound.tagId);
                    }
                }
                else {
                    var request = gapi.client.tagmanager.accounts.containers.workspaces.tags.create({ 'parent': workspace }, tag);
                    promisses.push(requestPromise(request));
                }
            }

            Promise.all(promisses).then((responses) => {
                tagsCreated = responses;
                return resolve();
            }).catch((ret) => {
                return reject(ret.message);
            });

        });
    }

    /*
     *  Update tag with trigger
     */ 
    function updateTagWithTrigger() {
        var promisses = [];

        return new Promise((resolve, reject) => {
            for (var i = 0; i < tagsCreated.length; i++) {

                var tag = tagsCreated[i];
                var trigger = [];

                if (!tag['firingTriggerId']) {

                    var triggerCreateFound = triggersCreated.find((tr) => { return tr.name === tag.name; });

                    if (triggerCreateFound) {
                        trigger = triggerCreateFound;
                    }
                    else {
                        var triggerExistFound = triggers.find((tr) => {return tr.name === tag.name });

                        if (!triggerExistFound)
                            throw "Não foi possível vincular a trigger na tag " + tag.name;

                        trigger = triggerExistFound;
                    }
                }

                tag['firingTriggerId'] = [trigger.triggerId];
                var request = gapi.client.tagmanager.accounts.containers.workspaces.tags.update({ 'path': tag.path }, tag);
                promisses.push(requestPromise(request));
            }

            Promise.all(promisses).then(() => {
                return resolve();
            }).catch((ret) => {
                return reject(ret.message);
            });

        });
    }

    /**
   * Creates variables.
   *
   */
    function createVariables(listVariables) {

        return new Promise((resolve, reject) => {

            var tmp_variables = listVariables.variable || [];

            var promisses = [];

            for (var i = 0; i < this.variables.length; i++) {


                // body request
                var variable = {
                    "name": variables[i].name,
                    "type": "v",
                    "parameter": [
                        {
                            "type": "template",
                            "key": "name",
                            "value": variables[i].gtm
                        }
                    ]
                };

                var variableFound = tmp_variables.find((currentValue) => {
                    return currentValue.name == variables[i].name;
                });

                // not exists variable in server
                if (variableFound) {

                    var haveChanges = diffVariable(variableFound, variables[i]);

                    if (haveChanges) {

                        //method update
                        var request = gapi.client.tagmanager.accounts.containers.workspaces.variables.update({ 'path': variableFound.path }, variable);
                        promisses.push(requestPromise(request));
                    }
                }
                else {
                    // dont exists variable in server, then create
                    var request = gapi.client.tagmanager.accounts.containers.workspaces.variables.create({ 'parent': workspace }, variable);
                    promisses.push(requestPromise(request));
                }
            }

            Promise.all(promisses)
                .then(() => {
                    return resolve();
                })
                .catch((ret) => {
                    return reject(ret.message);
                });

        });
    }

    /*
     * Check diff of data
     */
    function diffVariable(variableFound, variable) {
        return variableFound.notes !== variable.description || variableFound.parameter[0].value !== variable.gtm;
    }

    /*
     * Check diff of tag
     */
    function diffTag(tagFound, tag) {
        return tagFound.name !== tag.name || tagFound.parameter[0].value !== tag.value;
    }

    /*
     * List variables
     */
    function listVariables() {
        var request = gapi.client.tagmanager.accounts.containers.workspaces.variables.list({ 'parent': workspace });
        return requestPromise(request);
    }

    /*
     * List tags
     */
    function listTags(listTriggers) {
        triggers = listTriggers.trigger || [];
        var request = gapi.client.tagmanager.accounts.containers.workspaces.tags.list({'parent': workspace, 'fields': 'tag' });
        return requestPromise(request);
    }

    /*
     *  List triggers
     */ 
    function listTriggers() {
        var request = gapi.client.tagmanager.accounts.containers.workspaces.triggers.list({ 'parent': workspace });
        return requestPromise(request);
    }

    /**
    * Wraps an API request into a promise.
    *
    */
    function requestPromise(request) {
        return new Promise((resolve, reject) => {
            request.execute((response) => {
                if (response.code) {
                    reject(response);
                }
                resolve(response);
            });
        });
    }

    /*
   * Load api from tagManager
   */
    function loadTagManagerApi() {
        return new Promise((resolve, reject) => {
            gapi.client.load('tagmanager', 'v2', resolve);
        });
    }

</script>

@stop
