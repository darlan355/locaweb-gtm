<!DOCTYPE html>
<html class="ls-theme-green">
<head>
    <title>Gerenciador de TAGS</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta name="description" content="Insira aqui a descrição da página." />
    <link href="http://assets.locaweb.com.br/locastyle/3.10.1/stylesheets/locastyle.css" rel="stylesheet" type="text/css" />
    <link rel="icon" sizes="192x192" href="/locawebstyle/assets/images/ico-boilerplate.png" />
    <link rel="apple-touch-icon" href="/locawebstyle/assets/images/ico-boilerplate.png" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://malsup.github.io/min/jquery.blockUI.min.js"></script>
    <style type="text/css">
        body {
            overflow: hidden;
        }
    </style>

</head>
<body>




    <main>
        <div class="container-fluid">
            <h1 class="ls-title-intro ls-ico-tree">Integrações</h1>
            @yield('main')
        </div>
    </main>

    <div class="ls-modal" id="modal-erro">
        <div class="ls-modal-small">
            <div class="ls-modal-header">
                <button data-dismiss="modal">&times;</button>
                <h4 class="ls-modal-title">Atenção</h4>
            </div>
            <div class="ls-modal-body">
                <p id="pErro"></p>
            </div>
            <div class="ls-modal-footer">
                <button class="ls-btn ls-float-right" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>


    <script src="http://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>
    <script src="https://apis.google.com/js/client.js"></script>


    <script>


        function modalError(message) {
            dismissLoading();
            document.getElementById('pErro').innerText = message;
            locastyle.modal.open('#modal-erro');
        }

        var SCOPES = [
            'profile',
            'email',

            'https://www.googleapis.com/auth/tagmanager.manage.accounts',
            'https://www.googleapis.com/auth/tagmanager.edit.containers',
            'https://www.googleapis.com/auth/tagmanager.delete.containers',
            'https://www.googleapis.com/auth/tagmanager.edit.containerversions',
            'https://www.googleapis.com/auth/tagmanager.manage.users',
            'https://www.googleapis.com/auth/tagmanager.publish'
        ];



        gapi.load('auth2', function () {
            auth2 = gapi.auth2.init({
                client_id: '752648310447-leljc54e7nbgkcjtk4jgq5b2bqu1o7p6.apps.googleusercontent.com',
                fetch_basic_profile: true,
                scope: SCOPES.join(' ')
            });
        });


        function showLoading() {
            //$.blockUI({
            //    message: '<h3>Carregando ... </h3>',
            //    css: {
            //        height: '100px',

            //    }
            //});

            $.blockUI({ message: null });
        }

        function dismissLoading() {
            $.unblockUI();
        }

    </script>
</body>
</html>