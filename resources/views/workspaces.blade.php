@extends('integracoesmaster')

@section('main')

<div class="ls-alert-info ls-sm-space">Selecione um Workspace</div>

<div class="row" id="div-workspaces"></div>

<input type="hidden" id="hdUrl" value="<?php echo($url)?>" />

<style>
    .cursor {
        cursor: pointer;
    }
</style>


<script>
        var _token = '<?php  echo(csrf_token()); ?>';
</script>

<script type='text/javascript'>
    var accountPath = '';

    window.onload = function () {
        showLoading();

        loadTagManagerApi()
            .then(getAccount)
            .then(listContainers)
            .then(listWorkspaces)
            .then(buildWorkspacesHtml)
            .then(() => {
                dismissLoading();
            })
            .catch((ret) => {
                console.log(ret);
                modalError(ret.message);
        });

    }


    /*
    * Return account
    */
    function getAccount(){
            var request = gapi.client.tagmanager.accounts.list({});
            return requestPromise(request)
    }

    /*
     * Retorna promisse de containers
     */
    function listContainers(account) {

        accountPath = account.account[0].accountId;

        var request = gapi.client.tagmanager.accounts.containers.list({
            'parent': 'accounts/' + accountPath
        });

        return requestPromise(request)
            .then((response) => {
                var containers = response.container || [];
                console.log('containers');
                console.log(containers[0]);
                return containers[0];
            });
    }


    /**
 * Creates a workspace in the Greetings container.
 */
    function listWorkspaces(container) {
        var request = gapi.client.tagmanager.accounts.containers.workspaces.list({ 'parent': container.path });
        return requestPromise(request);
    }

    /*
     * Load api from tagManager
     */
    function loadTagManagerApi() {
        return new Promise((resolve, reject) => {
            debugger;
            gapi.client.load('tagmanager', 'v2', resolve);
        });
    }

    /*
     * Build workspaces in html
     */
    function buildWorkspacesHtml(result) {
        var html = '';
        var url = document.getElementById('hdUrl').value;
        
        var workspaces = result.workspace;

        for (var i = 0; i < workspaces.length; i++) {

            html += '<div class="col-md-3 ls-sm-space cursor">' +
                '<form action="' + url + '" method="post" onclick="showLoading();this.submit()">' +
                '<input type="hidden" name="workspace" value="' + workspaces[i].path + '" />' +
                '<input type="hidden" name="name_workspace" value="' + workspaces[i].name + '" />' +
                '<input type="hidden" name="_token" value="'+_token+'">'+
                '<div class="ls-box">' +
                '<h5 class="ls-title-3" >' + workspaces[i].name + '</h5>' +
                '<p></p>' +
                '</div>' +
                '</form>' +
                '</div>';

        }
        $('#div-workspaces').html(html);
    }


    /**
    * Wraps an API request into a promise.
    *
    */
    function requestPromise(request) {
        return new Promise((resolve, reject) => {
            request.execute((response) => {
                if (response.code) {
                    reject(response);
                }
                resolve(response);
            });
        });
    }
</script>


@stop