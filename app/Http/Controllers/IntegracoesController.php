<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;

class IntegracoesController extends Controller
{
    public function index() {

        $id_token =  session('id_token');
        $id = session('id');
        $id_user = session('id');
        $name_workspace = Request::input('name_workspace');
        $workspace =Request::input('workspace');

        if( !isset($id_user)  || !isset($name_workspace) || !isset($workspace))
            return redirect()->route('login');

        $url_trocarworkspace = route('listaWorkspaces',[ 'token'=>$id_token,  'id'=> $id ]);
        $url_tagspersonalizadas= route('listaIntegracoesTags', [ 'token'=>$id_token,  'id'=> $id ]);
        return view("integracoes")->with(['workspace_name' => $name_workspace, 'workspace' => $workspace, 'url_trocarworkspace'=>$url_trocarworkspace, 'url_tagspersonalizadas' => $url_tagspersonalizadas]);
    }

    public function dadosEcommerce(){
        $name = Request::input('ecommerce');
        $ecommerce = DB::table('gtm_ecommerces')->where('name', $name)->first();

        if(!isset($ecommerce))
            return response()->json([]);

        $tags =  DB::table('gtm_ecommerce_tag_values')
                                ->join('gtm_tags', 'gtm_tags.id', '=', 'gtm_ecommerce_tag_values.tagId')
                                ->where('gtm_ecommerce_tag_values.ecommerceId', $ecommerce->id)
                                 ->select('gtm_ecommerce_tag_values.*', 'gtm_tags.name')
                                ->get();

        $variables = DB::table('gtm_ecommerce_variables')->where('ecommerceId', $ecommerce->id)->get();

        return response()->json([
                'tags'=> $tags,
                'variables'=> $variables
            ]);
    }

    public function listaTagsIndex(){

        $id_user = session('id');

        if( !isset($id_user))
            return redirect()->route('login');


        $query = "
            select
                   gtm_tags_group.*,
	               gtm_group.name 'grupo',
                   gtm_tags.name 'tag'

            from  gtm_tags_group
            left join gtm_group on gtm_group.id= gtm_tags_group.groupId and gtm_group.status = 1
            left join gtm_tags on gtm_tags.id = gtm_tags_group.tagId and gtm_tags.status =1
            order by gtm_group.order
        ";

        $results = DB::select($query);
        $dados=[];
        $last_name_group = '';

        foreach ($results as $row)
        {
            $new = false;

            if($last_name_group == '' || $last_name_group !== $row->grupo)
            {
                $last_name_group = $row->grupo;
                $new=true;

                $rowData=[
                            'groupId'=>$row->groupId,
                            'grupo' => $last_name_group,
                            'tags' => [
                                [
                                   'tagId' => $row->tagId,
                                   'tag' => $row->tag,
                                ]
                            ]
                    ];
            }


        }




        return view('integracoestagspersonalizadas')->with(['dados'=> $dados]);
    }

}