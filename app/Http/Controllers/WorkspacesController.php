<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use Google_Client;
use Google_Service_TagManager;
use Illuminate\Support\Facades\DB;

class WorkspacesController extends Controller {

    public function index() {

        $id_token = Request::input('token');
        $id = Request::input('id');

        if (!isset($id_token))
            return redirect()->route('login');

        $CLIENT_ID = '752648310447-leljc54e7nbgkcjtk4jgq5b2bqu1o7p6.apps.googleusercontent.com';
        $client = new Google_Client(['client_id' => $CLIENT_ID]);

		$payload = $client->verifyIdToken($id_token);
		$expired= !$client->isAccessTokenExpired();
        if (!$payload && !$expired)
            return redirect()->route('login');

        // sub id para guardar no usu�rio se n�o existir
        $sub = $payload['sub'];
        $user = DB::table('users')->where('btgId', $sub);
        $id = 0;

        if($user->count() == 0){
            $id =DB::table('users')->insertGetId(
                ['btgId' => $sub]
            );
        }
        else
            $id = $user->first()->id;

        // Store a piece of data in the session...
        session(['id_token' => $id_token]);
        session(['id' => $id]);
        session(['user' => $payload]);

        $urlPostSelecionar = action('WorkspacesController@selecionar');
        return view('workspaces')->with( ['url' => $urlPostSelecionar]);
    }

    public function selecionar(){

        $id_user = session('id');

        $workspace =Request::input('workspace');
        $name_workspace=Request::input('name_workspace');
        if ( !isset($workspace) || !isset($id_user) )
            return redirect()->route('login');

        DB::table('user_workspaces')->insert( ['userId' =>$id_user, 'workspace' => $workspace] );
        return redirect()->route('listaIntegracoes', ['workspace'=>$workspace,'name_workspace'=>$name_workspace]);
    }

}
