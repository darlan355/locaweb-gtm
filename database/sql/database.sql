﻿-- MySQL Workbench Synchronization
-- Generated: 2018-03-13 16:49
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: marceloflores

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER SCHEMA `gtm_db`  DEFAULT CHARACTER SET utf8  DEFAULT COLLATE utf8_general_ci ;

CREATE TABLE IF NOT EXISTS `gtm_db`.`gtm_tags` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `description` VARCHAR(45) NULL DEFAULT NULL,
  `status` TINYINT(4) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `gtm_db`.`gtm_group` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `order` INT(11) NULL DEFAULT NULL,
  `status` TINYINT(4) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `gtm_db`.`gtm_object_models` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `tagId` INT(11) NOT NULL,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `placeholder` VARCHAR(255) NULL DEFAULT NULL,
  `required` TINYINT(4) NULL DEFAULT NULL,
  `type` VARCHAR(45) NULL DEFAULT NULL,
  `status` TINYINT(4) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_object_model_tags_idx` (`tagId` ASC),
  CONSTRAINT `fk_object_model_tags`
    FOREIGN KEY (`tagId`)
    REFERENCES `gtm_db`.`gtm_tags` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `gtm_db`.`gtm_object_values` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `tagsId` INT(11) NOT NULL,
  `objectModelId` INT(11) NOT NULL,
  `userWorkspacesId` INT(11) NOT NULL,
  `values` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_object_values_tags1_idx` (`tagsId` ASC),
  INDEX `fk_object_values_object_model1_idx` (`objectModelId` ASC),
  INDEX `fk_gtm_object_values_user_workspaces1_idx` (`userWorkspacesId` ASC),
  CONSTRAINT `fk_object_values_tags1`
    FOREIGN KEY (`tagsId`)
    REFERENCES `gtm_db`.`gtm_tags` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_object_values_object_model1`
    FOREIGN KEY (`objectModelId`)
    REFERENCES `gtm_db`.`gtm_object_models` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gtm_object_values_user_workspaces1`
    FOREIGN KEY (`userWorkspacesId`)
    REFERENCES `gtm_db`.`user_workspaces` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `gtm_db`.`gtm_object_models_values` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `objectModelId` INT(11) NOT NULL,
  `value` VARCHAR(45) NULL DEFAULT NULL,
  `key` VARCHAR(45) NULL DEFAULT NULL,
  `default` TINYINT(4) NOT NULL,
  `status` TINYINT(4) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_object_model_values_object_model1_idx` (`objectModelId` ASC),
  CONSTRAINT `fk_object_model_values_object_model1`
    FOREIGN KEY (`objectModelId`)
    REFERENCES `gtm_db`.`gtm_object_models` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `gtm_db`.`gtm_ecommerces` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `url` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `gtm_db`.`gtm_ecommerce_tag_values` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ecommerceId` INT(11) NOT NULL,
  `tagId` INT(11) NOT NULL,
  `value` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ecommerce_has_object_values_ecommerce1_idx` (`ecommerceId` ASC),
  INDEX `fk_ecommerce_tag_values_tags1_idx` (`tagId` ASC),
  CONSTRAINT `fk_ecommerce_has_object_values_ecommerce1`
    FOREIGN KEY (`ecommerceId`)
    REFERENCES `gtm_db`.`gtm_ecommerces` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ecommerce_tag_values_tags1`
    FOREIGN KEY (`tagId`)
    REFERENCES `gtm_db`.`gtm_tags` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `gtm_db`.`gtm_tags_group` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `tagId` INT(11) NOT NULL,
  `groupId` INT(11) NOT NULL,
  INDEX `fk_tags_has_group_group1_idx` (`groupId` ASC),
  INDEX `fk_tags_has_group_tags1_idx` (`tagId` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tags_has_group_tags1`
    FOREIGN KEY (`tagId`)
    REFERENCES `gtm_db`.`gtm_tags` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tags_has_group_group1`
    FOREIGN KEY (`groupId`)
    REFERENCES `gtm_db`.`gtm_group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `gtm_db`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `btgId` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `gtm_db`.`gtm_ecommerce_variables` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ecommerceId` INT(11) NOT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `gtm` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ecommerce_variables_ecommerce1_idx` (`ecommerceId` ASC),
  CONSTRAINT `fk_ecommerce_variables_ecommerce1`
    FOREIGN KEY (`ecommerceId`)
    REFERENCES `gtm_db`.`gtm_ecommerces` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `gtm_db`.`gtm_users_ecommerces` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ecommerceId` INT(11) NOT NULL,
  `userWorkspaceId` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_has_ecommerce_ecommerce1_idx` (`ecommerceId` ASC),
  INDEX `fk_gtm_users_ecommerces_user_workspaces1_idx` (`userWorkspaceId` ASC),
  CONSTRAINT `fk_users_has_ecommerce_ecommerce1`
    FOREIGN KEY (`ecommerceId`)
    REFERENCES `gtm_db`.`gtm_ecommerces` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gtm_users_ecommerces_user_workspaces1`
    FOREIGN KEY (`userWorkspaceId`)
    REFERENCES `gtm_db`.`user_workspaces` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `gtm_db`.`user_workspaces` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `userId` INT(11) NOT NULL,
  `workspace` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_workspaces_users1_idx` (`userId` ASC),
  CONSTRAINT `fk_user_workspaces_users1`
    FOREIGN KEY (`userId`)
    REFERENCES `gtm_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




INSERT INTO `gtm_db`.`gtm_ecommerces` (`name`, `description`, `url`) VALUES ('vtex', NULL, NULL);
INSERT INTO `gtm_db`.`gtm_ecommerces` (`name`, `description`, `url`) VALUES ('tray', NULL, NULL);
INSERT INTO `gtm_db`.`gtm_ecommerces` (`name`, `description`, `url`) VALUES ('fbits', NULL, NULL);
INSERT INTO `gtm_db`.`gtm_ecommerces` (`name`, `description`, `url`) VALUES ('rakuten', NULL, NULL);


/*
-- 0 inativo
-- 1 ativo
*/

INSERT INTO `gtm_db`.`gtm_tags` (`name`, `description`, `status`) VALUES ( 'Produto', NULL, 1);
INSERT INTO `gtm_db`.`gtm_tags` (`name`, `description`, `status`) VALUES ( 'Carrinho', NULL, 1);
INSERT INTO `gtm_db`.`gtm_tags` (`name`, `description`, `status`) VALUES ( 'Cliente', NULL, 1);
INSERT INTO `gtm_db`.`gtm_tags` (`name`, `description`, `status`) VALUES ( 'Pedido Finalizado', NULL, 1);

/* INSERT PRIMEIRO DA VTEX */
INSERT INTO `gtm_db`.`gtm_ecommerce_tag_values`(`ecommerceId`, `tagId`, `value`) VALUES ((select id from gtm_db.gtm_ecommerces where name ='vtex'), (select id from gtm_db.gtm_tags where name = 'Produto'), 'Carrinho de Controle');
INSERT INTO `gtm_db`.`gtm_ecommerce_tag_values`(`ecommerceId`, `tagId`, `value`) VALUES ((select id from gtm_db.gtm_ecommerces where name ='vtex'), (select id from gtm_db.gtm_tags where name = 'Produto'), 'Térmica de Chá');
INSERT INTO `gtm_db`.`gtm_ecommerce_tag_values`(`ecommerceId`, `tagId`, `value`) VALUES ((select id from gtm_db.gtm_ecommerces where name ='vtex'), (select id from gtm_db.gtm_tags where name = 'Cliente'), 'Pedro de Sá');
INSERT INTO `gtm_db`.`gtm_ecommerce_tag_values`(`ecommerceId`, `tagId`, `value`) VALUES ((select id from gtm_db.gtm_ecommerces where name ='vtex'), (select id from gtm_db.gtm_tags where name = 'Cliente'), 'João da Silva');

/* INSERT PRIMEIRO DA tray */
INSERT INTO `gtm_db`.`gtm_ecommerce_tag_values`(`ecommerceId`, `tagId`, `value`) VALUES ((select id from gtm_db.gtm_ecommerces where name ='tray'), (select id from gtm_db.gtm_tags where name = 'Produto'), 'Agua');
INSERT INTO `gtm_db`.`gtm_ecommerce_tag_values`(`ecommerceId`, `tagId`, `value`) VALUES ((select id from gtm_db.gtm_ecommerces where name ='tray'), (select id from gtm_db.gtm_tags where name = 'Produto'), 'Chuveiro');
INSERT INTO `gtm_db`.`gtm_ecommerce_tag_values`(`ecommerceId`, `tagId`, `value`) VALUES ((select id from gtm_db.gtm_ecommerces where name ='tray'), (select id from gtm_db.gtm_tags where name = 'Pedido Finalizado'), 'Número do pedido 100');

/* INSERT PRIMEIRO DA fbits */
INSERT INTO `gtm_db`.`gtm_ecommerce_tag_values`(`ecommerceId`, `tagId`, `value`) VALUES ((select id from gtm_db.gtm_ecommerces where name ='fbits'), (select id from gtm_db.gtm_tags where name = 'Produto'), 'Bolacha Recheada');
INSERT INTO `gtm_db`.`gtm_ecommerce_tag_values`(`ecommerceId`, `tagId`, `value`) VALUES ((select id from gtm_db.gtm_ecommerces where name ='fbits'), (select id from gtm_db.gtm_tags where name = 'Produto'), 'Cadeira de Praia');
INSERT INTO `gtm_db`.`gtm_ecommerce_tag_values`(`ecommerceId`, `tagId`, `value`) VALUES ((select id from gtm_db.gtm_ecommerces where name ='fbits'), (select id from gtm_db.gtm_tags where name = 'Pedido Finalizado'), 'Número do pedido 600');
INSERT INTO `gtm_db`.`gtm_ecommerce_tag_values`(`ecommerceId`, `tagId`, `value`) VALUES ((select id from gtm_db.gtm_ecommerces where name ='fbits'), (select id from gtm_db.gtm_tags where name = 'Cliente'), 'Alberto ROberto');


/*
  INSERT NAS VARIABLES
*/

INSERT INTO `gtm_db`.`gtm_ecommerce_variables`(`ecommerceId`,`name`,`description`,`gtm`)VALUES ((select id from gtm_db.gtm_ecommerces where name ='vtex'),'view','computa o número de views','tipo1');
INSERT INTO `gtm_db`.`gtm_ecommerce_variables`(`ecommerceId`,`name`,`description`,`gtm`)VALUES ((select id from gtm_db.gtm_ecommerces where name ='vtex'),'shared','compartilhamento de dados','tipo2');

INSERT INTO `gtm_db`.`gtm_ecommerce_variables`(`ecommerceId`,`name`,`description`,`gtm`)VALUES ((select id from gtm_db.gtm_ecommerces where name ='tray'),'view','computa o número de views','tipo1');
INSERT INTO `gtm_db`.`gtm_ecommerce_variables`(`ecommerceId`,`name`,`description`,`gtm`)VALUES ((select id from gtm_db.gtm_ecommerces where name ='tray'),'shared','compartilhamento de dados','tipo2');

INSERT INTO `gtm_db`.`gtm_ecommerce_variables`(`ecommerceId`,`name`,`description`,`gtm`)VALUES ((select id from gtm_db.gtm_ecommerces where name ='fbits'),'view','computa o número de views','tipo1');
INSERT INTO `gtm_db`.`gtm_ecommerce_variables`(`ecommerceId`,`name`,`description`,`gtm`)VALUES ((select id from gtm_db.gtm_ecommerces where name ='fbits'),'shared','compartilhamento de dados','tipo2');
INSERT INTO `gtm_db`.`gtm_ecommerce_variables`(`ecommerceId`,`name`,`description`,`gtm`)VALUES ((select id from gtm_db.gtm_ecommerces where name ='fbits'),'like','computa likes oferecidos pelos usuários','tipo3');


/*
* INSERT INTO GTM GROUP
*/

INSERT INTO `gtm_db`.`gtm_group`(`name`,`order`,`status`)VALUES('Navegação',1,1);
INSERT INTO `gtm_db`.`gtm_group`(`name`,`order`,`status`)VALUES('Pós Compras',2,1);
INSERT INTO `gtm_db`.`gtm_group`(`name`,`order`,`status`)VALUES('Lista de Desejos',3,1);
INSERT INTO `gtm_db`.`gtm_group`(`name`,`order`,`status`)VALUES('Recorrência',4,1);
INSERT INTO `gtm_db`.`gtm_group`(`name`,`order`,`status`)VALUES('Fidelidade',5,0);



/*
*  INSERT NOS GRUPOS DE TAGS
*/

INSERT INTO `gtm_db`.`gtm_tags_group`(`tagId`,`groupId`)VALUES((select id from gtm_db.gtm_tags where name ='Produto'),(select id from gtm_db.gtm_group where name ='Navegação'));
INSERT INTO `gtm_db`.`gtm_tags_group`(`tagId`,`groupId`)VALUES((select id from gtm_db.gtm_tags where name ='Carrinho'),(select id from gtm_db.gtm_group where name ='Navegação'));

INSERT INTO `gtm_db`.`gtm_tags_group`(`tagId`,`groupId`)VALUES((select id from gtm_db.gtm_tags where name ='Produto'),(select id from gtm_db.gtm_group where name ='Pós Compras'));
INSERT INTO `gtm_db`.`gtm_tags_group`(`tagId`,`groupId`)VALUES((select id from gtm_db.gtm_tags where name ='Carrinho'),(select id from gtm_db.gtm_group where name ='Pós Compras'));
INSERT INTO `gtm_db`.`gtm_tags_group`(`tagId`,`groupId`)VALUES((select id from gtm_db.gtm_tags where name ='Pedido Finalizado'),(select id from gtm_db.gtm_group where name ='Pós Compras'));
